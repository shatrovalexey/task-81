use bignum ;

*{ 'x!' } = sub (
	$x
	Факториал числа первого аргумента
) {
	my ( $result , $x ) = ( shift ) x 2 ;

	$result *= -- $x while $x > 1 ;
	$result
} ;

*{ 'max( x ): not b! % ( a ^ x )' } = sub (
	$a and $b : max( x ) : not b! % ( a ^ x )
	Это - наибольшее целочисленное значение X, такое, что A ^ X делит B!
) {
	my ( $a , $b ) = @_ ;
	( local ${ 'b!' } , my $x ) = ( 'x!'->( $b ) ) x 2 ;

	${ 'b!' } % ( $a ^ ( $x + 1 ) ) or return $x while $x -- > 1
} ; {
	# $a = 7 , $b = 14
	( my ( $a , $b ) , local $\ ) = ( 7 , 14 , "\n" ) ;

	print 'max( x ): not b! % ( a ^ x )'->( $a , $b )
}