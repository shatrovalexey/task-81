<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Category ;
use App\Entity\Product ;

class AppFixtures extends Fixture {
    public function load( ObjectManager $manager ) {
		for ( $i = rand( 1e1 , 1e2 ) ; $i >= 0 ; $i -- ) {
			$category = new Category( ) ;

			$category->setTitle( sha1( uniqid( ) ) ) ;
			$category->setEId( rand( 1 , 1e2 ) ) ;

			$manager->persist( $category ) ;

			for ( $j = rand( 1 , 1e2 ) ; $j >= 0 ; $j -- ) {
				$product = new Product( ) ;

				$product->setTitle( sha1( uniqid( ) ) ) ;
				$product->setCategory( $category ) ;
				$product->setEId( rand( 1 , 1e2 ) ) ;
				$product->setPrice( rand( 1 , 1e6 ) / 1e2 ) ;

				$manager->persist( $product ) ;
			}
		}

        $manager->flush( ) ;
    }
}