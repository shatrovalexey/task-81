<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\Category ;
use App\Entity\Product ;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry ;

class LoadCommand extends Command {
	/**
	* @var string
	*/
    protected static $defaultName = 'Load' ;

	/**
	* @var Doctrine\Common\Persistence\ObjectManager
	*/
	private $manager ;

	public function __construct( ManagerRegistry $manager ) {
		$this->manager = $manager ;
	}

    protected function configure( ) {
        $this->setDescription( 'Set categoryes or products JSON file' )
			->addArgument( 'categoryes' , InputArgument::OPTIONAL , 'categoryes JSON' )
			->addArgument( 'products' , InputArgument::OPTIONAL , 'products JSON' ) ;
    }

    protected function execute( InputInterface $input , OutputInterface $output ) : int {
		$category = $this->getRepository( 'category' ) ;

        if ( $category_file = $input->getArgument( 'categoryes' ) ) {
			$this->from_json( $category_file , function( $item ) {
				$category = new Category( ) ;

				$category->setTitle( $item->title ) ;
				$category->setEId( $item->eId ) ;

				$this->manager->persists( $category ) ;
			} ) ;

	        $output->success( 'Categories loaded' ) ;
        }
        if ( $product_file = $input->getArgument( 'products' ) ) {
			$this->from_json( $product_file , function( $item ) {
				$category = Category::find( $item->category )->getOne( ) ;

				$product = new Product( ) ;

				$product->setTitle( $item->title ) ;
				$product->setEId( $item->eId ) ;
				$product->setPrice( $item->price ) ;
				$product->setCategory( $category ) ;

				$this->manager->persists( $product ) ;
			} ) ;

	        $output->success( 'Products loaded' ) ;
        }

        return 0 ;
    }

	protected function from_json( $file_name , $callback ) {
		echo getcwd( ) . PHP_EOL ;
		$data = file_get_contents( $file_name ) ;

		if ( empty( $data ) ) {
			throw new \Exception( 'Data from "' . $file_name . '" is empty' ) ;
		}

		$data = json_decode( $data ) ;

		if ( empty( $data ) ) {
			throw new \Exception( 'Data from "' . $file_name . '" is not JSON encoded' ) ;
		}

		if ( ! is_array( $data ) ) {
			throw new \Exception( 'Data from "' . $file_name . '" is not array' ) ;
		}

		foreach ( $data as $item ) {
			$callback( $item ) ;
		}
	}
}
