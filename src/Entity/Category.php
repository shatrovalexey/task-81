<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\ORM\Mapping as ORM ;
use Doctrine\Common\Collections\ArrayCollection ;
use Doctrine\Common\Collections\Collection ;

/**
* @ORM\Entity(repositoryClass=CategoryRepository::class)
*/
class Category {
	/**
	* @ORM\Id()
	* @ORM\GeneratedValue()
	* @ORM\Column(type="integer")
	*/
	protected $id ;

	/**
	* @ORM\Column(type="string", nullable=false)
	*/
	protected $title ;

	/**
	* @ORM\Column(type="integer", nullable=false)
	*/
	protected $eId ;

	/**
	* @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="category", cascade={"remove"})
	* @ORM\JoinColumn(nullable=false)
	*/
	protected $products ;

    /**
     * Constructor.
     */
    public function __construct( ) {
		$this->products = new \Doctrine\Common\Collections\ArrayCollection( ) ;
	}

	/**
	* @return integer
	*/
	public function getId( ): ?int {
		return $this->id ;
	}

	/**
	* @return string
	*/
	public function getTitle( ) {
		return $this->title ;
	}

	/**
	* @param string $title - название
	*/
	public function setTitle( $title ) : void {
		$this->title = $title ;
	}

	/**
	* @param integer $eId - идентификатор "e"
	*
	* @return integer
	*/
	public function getEId( ) {
		return $this->eId ;
	}

	/**
	* @param integer $eId - идентификатор "e"
	*/
	public function setEId( $eId ) : void {
		$this->eId = $eId ;
	}

	/**
	* @return \Doctrine\Common\Collections\Collection
	*/
	public function getProducts( ): Collection {
		return $this->products ;
	}

	/**
	* Add product.
	*
	* @param \App\Entity\Product $product
	*
	* @return Ethnicity
	*/
	public function addMember( \App\Entity\Product $product ) {
		$this->products[ ] = $product ;

		return $this ;
	}

	/**
	* Remove product.
	*
	* @param \App\Entity\Product $product
	*/
	public function removeMember( $product ) {
		$this->products->removeElement( $product ) ;
	}

	public function __toString( ) {
		return $this->getTitle( ) ;
	}
}