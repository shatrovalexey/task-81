<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product {
	/**
	* @ORM\Id()
	* @ORM\GeneratedValue()
	* @ORM\Column(type="integer")
	*/
	protected $id ;

	/**
	* @ORM\Column(type="string", nullable=false)
	*/
	protected $title ;

	/**
	* @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="products")
	*/
	protected $category ;

	/**
	* @ORM\Column(type="integer", nullable=false)
	*/
	protected $eId ;

	/**
	* @ORM\Column(type="decimal", nullable=false , precision = 6 , scale = 2 )
	*/
	protected $price ;

	/**
	* @return integer
	*/
	public function getId( ) : ?int {
		return $this->id ;
	}

	/**
	* @return string
	*/
	public function getTitle( ) {
		return $this->title ;
	}

	/**
	* @param string $title - название
	*/
	public function setTitle( $title ) : void {
		$this->title = $title ;
	}

	/**
	* @return \App\Entity\Category
	*/
	public function getCategory( ) {
		return $this->category ;
	}

	/**
	* @param \App\Entity\Category $category - идентификатор категории
	*/
	public function setCategory( \App\Entity\Category $category ) : void {
		$this->category = $category ;
	}

	/**
	* @param integer $eId - идентификатор "e"
	*
	* @return integer
	*/
	public function getEId( ) {
		return $this->eId ;
	}

	/**
	* @param integer $eId - идентификатор "e"
	*/
	public function setEId( $eId ) : void {
		$this->eId = $eId ;
	}

	/**
	* @return double
	*/
	public function getPrice( ) {
		return $this->price ;
	}

	/**
	* @param double $price - цена
	*/
	public function setPrice( $price ) : void {
		$this->price = $price ;
	}

	public function _toString( ) {
		return $this->getTitle( ) ;
	}
}